#! /bin/bash
cd api && yarn install && yarn run build && cd .. \
&& cd ui && yarn install && yarn run build && cd .. \
&& docker-compose build && docker-compose up -d