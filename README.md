# Demo ReactJS - NODEJS

## How run application

```
- Run with manuall
- clone repo
- cd api && yarn install && cd ..
- cd ui && yarn install && cd ..
- docker-compose build && docker-compose up -d
- waitting service MONGODB - API - UI start
```

```
- Run with script 
- ./start.sh
- waitting service MONGODB - API - UI start
- Open browser with address http://0.0.0.0:3000
- Login with email: admin@mail.com - password: 123
```

## API

```
- Service start with default port 5000
- cd api && yarn install
- Run outsite docker: yarn run dev
- Run test: yarn run test
- Show coverage: yarn run coverage
- Build for production: yarn run build
```

```
- Login API

curl -X POST \
  http://0.0.0.0:5000/api/user/login \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content: application/json' \
  -d '{
	"email": "admin@mail.com",
	"password" : "123"
}'
```

```
- Get list post

curl -X GET \
  http://0.0.0.0:5000/api/post \
  -H 'Authorization: replace_token_from_api_login' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content: application/json' \
```

```
- Get post detail

curl -X GET \
  http://0.0.0.0:5000/api/post/post_id \
  -H 'Authorization: replace_token_from_api_login' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content: application/json' \
```

## UI

```
- Service start with default port 3000
- cd ui && yarn install
- Run outsite docker: yarn run start
- Run test: yarn run test
- Show coverage: yarn run test --coverage
- Build for production: yarn run build
```