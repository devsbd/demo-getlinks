import mongoose from "mongoose";
import { autoCreateListPost } from "controllers/post";
import { autoCreateUser } from "controllers/user";
import config from "./config";

mongoose.Promise = global.Promise;
mongoose.set("useCreateIndex", true);

mongoose.connection.on('connected',() => {
  console.log('mongodb connect success');
  autoCreateListPost(16);
  autoCreateUser();
});

const connection = mongoose.connect(
  config.DB_URI,
  { useNewUrlParser: true , autoReconnect: true}
);

connection
  .then(db => {
    return db;
  })
  .catch(err => {
    if (err.message.code === "ETIMEDOUT") {
      console.error("Attempting to re-establish database connection.");
    } else {
      console.error("Error while attempting to connect to database:");
    }
  });

export default connection;
