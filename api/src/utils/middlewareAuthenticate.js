import jwt from "jsonwebtoken";
import config from "utils/config";

const middlewareAuthenticate = async (req, res, next) => {
  const { authorization } = req.headers;
  if (authorization) {
    jwt.verify(authorization, config.JWT_SECRET, (error, decoded) => {
      if (!error) {
        req.userAuth = decoded;
      }
      next();
    });
  } else {
    next();
  }
};

exports.middlewareAuthenticate = middlewareAuthenticate;
