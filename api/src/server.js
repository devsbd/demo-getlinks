import express from "express";
import bodyParser from "body-parser";
import fs from "fs";
import path from "path";
import { middlewareAuthenticate } from "utils/middlewareAuthenticate";
import config from "utils/config";
import cors from "cors";

const app = express();

app.use(middlewareAuthenticate);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.listen(config.PORT, () => {
  console.log(`Node server listening on port ${config.PORT}`);

  require("utils/mongoConnection");
  fs.readdirSync(path.join(__dirname, "routes")).map(file => {
    require("./routes/" + file)(app);
  });
});
