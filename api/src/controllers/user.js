import User from "models/user";
import bcrypt from "bcrypt";
import config from "utils/config";
import jwt from "jsonwebtoken";
import { isEmail, isEmpty } from "validator";

const autoCreateUser = async () => {
  return User.findOne({ email: config.EMAIL_ADMIN })
    .then(data => {
      if (!data) {
        const newUser = new User({
          email: config.EMAIL_ADMIN,
          password: bcrypt.hashSync(config.EMAIL_PASSWORD, config.PASSWORD_SALT)
        });

        return newUser.save().then(data => data);
      }
      return false;
    })
    .catch(error => {
      console.error(`Error create admin account ${error.message}`);
    });
};

exports.autoCreateUser = autoCreateUser;

exports.login = async (req, res) => {
  const { email, password } = req.body || {};
  if (
    !email ||
    !password ||
    isEmpty(email) ||
    !isEmail(email) ||
    isEmpty(password)
  ) {
    return res.status(401).json({
      message: "Email or password invalid"
    });
  }
  return User.findOne({ email: email })
    .then(data => {
      if (data && bcrypt.compareSync(password, data.password)) {
        res.status(200).json({
          email: data.email,
          token: jwt.sign({ user_id: data._id }, config.JWT_SECRET)
        });
      } else {
        res
          .status(401)
          .json({ message: "Email not exists or password incorrect" });
      }
    })
    .catch(error => {
      console.error(`Error create admin account ${error.message}`);
    });
};
