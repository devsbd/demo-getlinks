import Post from "models/post";
import faker from "faker";

const autoCreateListPost = async len => {
  for (let i = 0; i < len; i++) {
    const post = new Post({
      title: faker.name.title(),
      content: faker.lorem.paragraphs(15,'<br /> <br />'),
      image: "https://i.imgur.com/UIKB7S6.jpg"
    });
    await post.save();
  }
};

exports.autoCreateListPost = autoCreateListPost;

exports.listPost = (req, res) => {
  const { userAuth } = req;
  if (!userAuth) {
    return res.status(401).json({ message: "User not authenticate" });
  }

  const limit = 16;
  return Post.find()
    .limit(limit)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(error => {
      console.error(`Error api get list post ${error.message}`);
    });
};

exports.post = async (req, res) => {
  const { id } = req.params || {};
  const { userAuth } = req;

  if (!userAuth) {
    return res.status(401).json({ message: "User not authenticate" });
  }

  return Post.findOne({ _id: id })
    .then(data => {
      return res.status(200).json(data);
    })
    .catch(error => {
      console.error(`Error api get post detail ${error.message}`);
      return res.status(400).json({ message: "Post is not exists" });
    });
};
