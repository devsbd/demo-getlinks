import mongoose from "mongoose";

const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true }
});

userSchema.methods = {};

export default mongoose.models.user || mongoose.model("user", userSchema);
