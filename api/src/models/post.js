import mongoose from "mongoose";

const Schema = mongoose.Schema;

const postSchema = new Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  image: { type: String, required: true }
});

postSchema.methods = {};

export default mongoose.models.post || mongoose.model("post", postSchema);
