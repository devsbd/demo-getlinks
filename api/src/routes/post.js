import post from "controllers/post";

module.exports = api => {
  api.route("/api/post").get(post.listPost);
  api.route("/api/post/:id").get(post.post);
};
