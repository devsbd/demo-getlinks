import user from "controllers/user";

module.exports = api => {
  api.route("/api/user/login").post(user.login);
};
