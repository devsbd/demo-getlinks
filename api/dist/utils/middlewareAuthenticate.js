"use strict";

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _config = _interopRequireDefault(require("./config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

const middlewareAuthenticate =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(function* (req, res, next) {
    const {
      authorization
    } = req.headers;

    if (authorization) {
      _jsonwebtoken.default.verify(authorization, _config.default.JWT_SECRET, (error, decoded) => {
        if (!error) {
          req.userAuth = decoded;
        }

        next();
      });
    } else {
      next();
    }
  });

  return function middlewareAuthenticate(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.middlewareAuthenticate = middlewareAuthenticate;