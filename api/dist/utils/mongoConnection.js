"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _post = require("../controllers/post");

var _user = require("../controllers/user");

var _config = _interopRequireDefault(require("./config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose.default.Promise = global.Promise;

_mongoose.default.set("useCreateIndex", true);

_mongoose.default.connection.on('connected', () => {
  console.log('mongodb connect success');
  (0, _post.autoCreateListPost)(16);
  (0, _user.autoCreateUser)();
});

const connection = _mongoose.default.connect(_config.default.DB_URI, {
  useNewUrlParser: true,
  autoReconnect: true
});

connection.then(db => {
  return db;
}).catch(err => {
  if (err.message.code === "ETIMEDOUT") {
    console.error("Attempting to re-establish database connection.");
  } else {
    console.error("Error while attempting to connect to database:");
  }
});
var _default = connection;
exports.default = _default;