"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const config = {
  DB_HOST: process.env.DB_HOST || "0.0.0.0",
  DB_NAME: process.env.DB_NAME || "getlinks_db",
  DB_USER_NAME: process.env.DB_USER_NAME || "root",
  DB_USER_PASS: process.env.DB_USER_PASS || "root",
  DB_PORT: process.env.DB_PORT || "27017",
  PORT: process.env.PORT || 5000,
  EMAIL_ADMIN: process.env.EMAIL_ADMIN || "admin@mail.com",
  EMAIL_PASSWORD: process.env.EMAIL_PASSWORD || "123",
  PASSWORD_SALT: parseInt(process.env.PASSWORD_SALT, 10) || 10,
  JWT_SECRET: process.env.JWT_SECRET || "!#!#!#!#!&$*!@#&$!#1"
};
config.DB_URI = `mongodb://${config.DB_USER_NAME}:${config.DB_USER_PASS}@${config.DB_HOST}:${config.DB_PORT}/${config.DB_NAME}`;
var _default = config;
exports.default = _default;