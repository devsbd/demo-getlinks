"use strict";

var _post = _interopRequireDefault(require("../models/post"));

var _faker = _interopRequireDefault(require("faker"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

const autoCreateListPost =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(function* (len) {
    for (let i = 0; i < len; i++) {
      const post = new _post.default({
        title: _faker.default.name.title(),
        content: _faker.default.lorem.paragraphs(15, '<br /> <br />'),
        image: "https://i.imgur.com/UIKB7S6.jpg"
      });
      yield post.save();
    }
  });

  return function autoCreateListPost(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.autoCreateListPost = autoCreateListPost;

exports.listPost = (req, res) => {
  const {
    userAuth
  } = req;

  if (!userAuth) {
    return res.status(401).json({
      message: "User not authenticate"
    });
  }

  const limit = 16;
  return _post.default.find().limit(limit).then(data => {
    res.status(200).json(data);
  }).catch(error => {
    console.error(`Error api get list post ${error.message}`);
  });
};

exports.post =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(function* (req, res) {
    const {
      id
    } = req.params || {};
    const {
      userAuth
    } = req;

    if (!userAuth) {
      return res.status(401).json({
        message: "User not authenticate"
      });
    }

    return _post.default.findOne({
      _id: id
    }).then(data => {
      return res.status(200).json(data);
    }).catch(error => {
      console.error(`Error api get post detail ${error.message}`);
      return res.status(400).json({
        message: "Post is not exists"
      });
    });
  });

  return function (_x2, _x3) {
    return _ref2.apply(this, arguments);
  };
}();