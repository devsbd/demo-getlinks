"use strict";

var _user = _interopRequireDefault(require("../models/user"));

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _config = _interopRequireDefault(require("../utils/config"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _validator = require("validator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

const autoCreateUser =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(function* () {
    return _user.default.findOne({
      email: _config.default.EMAIL_ADMIN
    }).then(data => {
      if (!data) {
        const newUser = new _user.default({
          email: _config.default.EMAIL_ADMIN,
          password: _bcrypt.default.hashSync(_config.default.EMAIL_PASSWORD, _config.default.PASSWORD_SALT)
        });
        return newUser.save().then(data => data);
      }

      return false;
    }).catch(error => {
      console.error(`Error create admin account ${error.message}`);
    });
  });

  return function autoCreateUser() {
    return _ref.apply(this, arguments);
  };
}();

exports.autoCreateUser = autoCreateUser;

exports.login =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(function* (req, res) {
    const {
      email,
      password
    } = req.body || {};

    if (!email || !password || (0, _validator.isEmpty)(email) || !(0, _validator.isEmail)(email) || (0, _validator.isEmpty)(password)) {
      return res.status(401).json({
        message: "Email or password invalid"
      });
    }

    return _user.default.findOne({
      email: email
    }).then(data => {
      if (data && _bcrypt.default.compareSync(password, data.password)) {
        res.status(200).json({
          email: data.email,
          token: _jsonwebtoken.default.sign({
            user_id: data._id
          }, _config.default.JWT_SECRET)
        });
      } else {
        res.status(401).json({
          message: "Email not exists or password incorrect"
        });
      }
    }).catch(error => {
      console.error(`Error create admin account ${error.message}`);
    });
  });

  return function (_x, _x2) {
    return _ref2.apply(this, arguments);
  };
}();