"use strict";

var _express = _interopRequireDefault(require("express"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _middlewareAuthenticate = require("./utils/middlewareAuthenticate");

var _config = _interopRequireDefault(require("./utils/config"));

var _cors = _interopRequireDefault(require("cors"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express.default)();
app.use(_middlewareAuthenticate.middlewareAuthenticate);
app.use(_bodyParser.default.json());
app.use(_bodyParser.default.urlencoded({
  extended: false
}));
app.use((0, _cors.default)());
app.listen(_config.default.PORT, () => {
  console.log(`Node server listening on port ${_config.default.PORT}`);

  require("./utils/mongoConnection");

  _fs.default.readdirSync(_path.default.join(__dirname, "routes")).map(file => {
    require("./routes/" + file)(app);
  });
});