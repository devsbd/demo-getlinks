"use strict";

var _post = _interopRequireDefault(require("../controllers/post"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = api => {
  api.route("/api/post").get(_post.default.listPost);
  api.route("/api/post/:id").get(_post.default.post);
};