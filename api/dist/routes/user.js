"use strict";

var _user = _interopRequireDefault(require("../controllers/user"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = api => {
  api.route("/api/user/login").post(_user.default.login);
};