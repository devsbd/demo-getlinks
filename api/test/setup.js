const prepare = require("mocha-prepare");
const mongoUnit = require("mongo-unit");
const mongoose = require("mongoose");
mongoose.set("useCreateIndex", true);

prepare(done =>
  mongoUnit.start().then(testMongoUrl => {
    process.env.MONGO_URL = testMongoUrl;
    done();
  })
);
