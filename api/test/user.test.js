const chai = require("chai");
const mongoUnit = require("mongo-unit");
const { mockRequest, mockResponse } = require("mock-req-res");
const userController = require("../src/controllers/user");
const config = require("../src/utils/config").default;
const User = require("../src/models/user").default;
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const expect = chai.expect;
const { stub, match } = require("sinon");
const sinonChai = require("sinon-chai");
chai.use(sinonChai);

describe("Test user controller", () => {
  const res = mockResponse();

  before(() =>
    mongoUnit.start().then(url =>
      mongoose.connect(
        url,
        { useNewUrlParser: true }
      )
    )
  );

  it("should user login success", async () => {
    await userController.autoCreateUser();
    const newUser = await User.findOne({ email: config.EMAIL_ADMIN }).then(
      data => data
    );
    const expectJson = {
      email: config.EMAIL_ADMIN,
      token: jwt.sign({ user_id: newUser._id }, config.JWT_SECRET)
    };
    const req = mockRequest({
      body: { email: config.EMAIL_ADMIN, password: config.EMAIL_PASSWORD }
    });

    await userController.login(req, res);
    expect(res.json).to.have.been.calledWith(expectJson);
  });

  it("should user admin don't duplicate", async () => {
    const result = await userController.autoCreateUser();
    expect(result).to.equal(false);
  });

  it("should user login fail", async () => {
    const expectJson = {
      message: "Email not exists or password incorrect"
    };

    const req = mockRequest({
      body: { email: "abc@gmail.com", password: "1" }
    });

    await userController.login(req, res);
    expect(res.json).to.have.been.calledWith(expectJson);
  });

  describe("with validate data", () => {
    const expectJson = {
      message: "Email or password invalid"
    };
    it("should error with empty body", async () => {
      let req = mockRequest({
        body: {}
      });
      await userController.login(req, res);
      expect(res.json).to.have.been.calledWith(expectJson);
    });

    it("should error with invalid email", async () => {
      let req = mockRequest({
        body: { email: "abc" }
      });
      await userController.login(req, res);
      expect(res.json).to.have.been.calledWith(expectJson);
    });

    it("should error with invalid password", async () => {
      let req = mockRequest({
        body: { password: "" }
      });
      await userController.login(req, res);
      expect(res.json).to.have.been.calledWith(expectJson);
    });
  });
});
