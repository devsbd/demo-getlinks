const chai = require("chai");
const mongoUnit = require("mongo-unit");
const { mockRequest, mockResponse } = require("mock-req-res");
const postController = require("../src/controllers/post");
const Post = require("../src/models/post").default;
const mongoose = require("mongoose");
const expect = chai.expect;
const { stub, match } = require("sinon");
const sinonChai = require("sinon-chai");
chai.use(sinonChai);

const userAuth = {
  _id: 1
};

describe("Test post controller", () => {
  const res = mockResponse();
  before(() =>
    mongoUnit
      .start()
      .then(url =>
        mongoose.connect(
          url,
          { useNewUrlParser: true }
        )
      )
      .then(() => postController.autoCreateListPost(10))
  );

  afterEach(() => {
    res.json.resetHistory();
  });

  it("should auto create list post", async () => {
    const countPost = 10;
    Post.find().then(data => {
      expect(countPost).to.equal(data.length);
    });
  });

  it("should get list post with authenticate", async () => {
    const req = mockRequest({
      userAuth: userAuth
    });
    await postController.listPost(req, res);
    expect(res.status).to.have.been.calledWith(200);
  });

  it("should can't get list post without authenticate", async () => {
    const req = mockRequest({
      userAuth: null
    });
    await postController.listPost(req, res);
    expect(res.status).to.have.been.calledWith(401);
    expect(res.json).to.have.been.calledWith({ message: "User not authenticate" });
  });

  it("should get post detail with authenticate", async () => {
    const post = await Post.findOne().then(data => data);
    const req = mockRequest({
      params: { id: post._id },
      userAuth: userAuth
    });
    await postController.post(req, res);
    expect(res.status).to.have.been.calledWith(200);
    expect(res.json).to.have.been.calledWith(post);
  });

  it("should can't get post detail with wrong id", async () => {
    const req = mockRequest({
      params: { id: "kfaifuasdfjasdfj" },
      userAuth: userAuth
    });
    await postController.post(req, res);
    expect(res.status).to.have.been.calledWith(400);
    expect(res.json).to.have.been.calledWith({ message: "Post is not exists" });
  });

  it("should can't get post detail without authenticate", async () => {
    const post = await Post.findOne().then(data => data);
    const req = mockRequest({
      params: { id: post._id },
      userAuth: null
    });
    await postController.post(req, res);
    expect(res.status).to.have.been.calledWith(401);
    expect(res.json).to.have.been.calledWith({
      message: "User not authenticate"
    });
  });
});
