const chai = require("chai");
const mongoUnit = require("mongo-unit");
const { mockRequest, mockResponse } = require("mock-req-res");
const config = require("../src/utils/config").default;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const expect = chai.expect;
const sinonChai = require("sinon-chai");
const { middlewareAuthenticate } = require("utils/middlewareAuthenticate");
const sinon = require("sinon");
chai.use(sinonChai);

describe("Test middleware", () => {
  it("should verify token success", async () => {
    const userInfo = { user_id: 10 };
    const token = jwt.sign(userInfo, config.JWT_SECRET);
    const req = mockRequest({
      headers: {
        authorization: token
      }
    });
    await middlewareAuthenticate(req, {}, () => {});
    expect(req).to.be.property("userAuth");
  });

  it("should verify token fail with wrong token", async () => {
    const req = mockRequest({
      headers: {
        authorization: "123"
      }
    });
    await middlewareAuthenticate(req, {}, () => {});
    expect(req.headers).not.property("userAuth");
  });

  it("should ignore without authorization on headers", async () => {
    const req = mockRequest({ headers: {} });
    const next = sinon.spy();
    await middlewareAuthenticate(req, {}, next);
    expect(next).to.have.calledOnce;
  });
});
