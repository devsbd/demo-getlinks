import React from "react";
import { Link } from "react-router-dom";

const CardItem = ({ title, image, id }) => (
  <div className="card__item">
    <Link to={"card-detail/" + id} className="custom-link">
      <div className="card__item--picture--box">
        <img src={image} alt={title} className="card__item--picture" />
      </div>

      <div className="card__item--description">
        <h3 className="nowrap">{title}</h3>
      </div>
    </Link>
  </div>
);

export default CardItem;
