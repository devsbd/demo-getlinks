import React from "react";

const Button = ({ className, content, onClick }) => (
  <button className={className} onClick={onClick}>
    {content}
  </button>
);

export default Button;
