import React from "react";

const Input = ({
  type = "text",
  className = "form__input",
  placeholder,
  id,
  required = false,
  onChange,
  value = ""
}) => (
  <input
    type={type}
    className={className}
    placeholder={placeholder}
    id={id}
    required={required}
    onChange={onChange}
    value={value}
  />
);

export default Input;
