import { Home, CardDetail } from "src/pages";

const routes = [
  {
    key: "route-home",
    path: "/",
    exact: true,
    component: Home
  },
  {
    key: "card-detail",
    path: "/card-detail/:id",
    exact: true,
    component: CardDetail
  }
];

export default routes;
