import React from "react";
import CardItem from "src/components/card-item";

export default class Cards extends React.Component {
  renderData() {
    const { data } = this.props;
    if (data.length > 0) {
      return data.map(item => (
        <div className="col-sm-6 col-lg-3" key={item._id}>
          <div className="card">
            <CardItem title={item.title} image={item.image} id={item._id} />
          </div>
        </div>
      ));
    }
    return null;
  }

  render() {
    return (
      <div className="container u-margin-top-big">
        <div className="row">{this.renderData()}</div>
      </div>
    );
  }
}
