import React from "react";
import { Button, Input } from "src/components";

export default class Form extends React.Component {
  state = {
    email: "",
    password: ""
  };

  handleChange = (e, key) => {
    this.setState({
      [key]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.handleSubmit(this.state);
  };

  render() {
    const { messageError } = this.props;
    return (
      <section className="section-login u-margin-top-big ">
        <form className="form">
          <div className="u-margin-bottom-medium">
            <h2 className="heading-secondary">Login</h2>
          </div>

          <div className="form__group">
            <Input
              type="text"
              className="form__input"
              placeholder="Username"
              id="name"
              required
              onChange={e => this.handleChange(e, "email")}
              value={this.state.email}
            />

            <label htmlFor="name" className="form__label">
              Username
            </label>
          </div>

          <div className="form__group">
            <Input
              type="password"
              className="form__input"
              placeholder="Password"
              id="password"
              required
              onChange={e => this.handleChange(e, "password")}
              value={this.state.password}
            />

            <label htmlFor="password" className="form__label">
              Password
            </label>
          </div>

          {messageError && (
            <div className="form__group">
              <div className="form__alert form__alert--warning">
                {messageError}
              </div>
            </div>
          )}

          <div className="form__group">
            <Button
              className="btn btn--green"
              content="Login"
              onClick={this.handleSubmit}
            />
          </div>
        </form>
      </section>
    );
  }
}
