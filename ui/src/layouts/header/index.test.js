import React from "react";
import Header from "./index";
import { mount } from "enzyme";

test("Header render", () => {
  const Wrapper = mount(<Header />);
  expect(Wrapper.find(".header").exists()).toBe(true);
});
