import React from "react";
import logo from "src/images/nat-1.jpg";

const HeaderSection = () => (
  <header className="header">
    <div className="header__logo-box">
      <img src={logo} alt="logo" className="header__logo" />
    </div>

    <div className="header__link-box">
      <span className="header__link">Text 1</span>
      <span className="header__link">Text 2</span>
      <span className="header__link">Text 3</span>
      <span className="header__link">Text 3</span>
    </div>
  </header>
);

export default HeaderSection;
