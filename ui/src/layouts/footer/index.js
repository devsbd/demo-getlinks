import React from "react";

const Footer = () => (
  <section className="footer">
    <div className="container">
      <div className="row">
        <div className="col-md-3">
          <h3 className="heading-tertiary">Something</h3>
          <p className="paragraph">something</p>
          <p className="paragraph">something</p>
          <p className="paragraph">something</p>
        </div>

        <div className="col-md-3">
          <h3 className="heading-tertiary">Something</h3>
          <p className="paragraph">something</p>
          <p className="paragraph">something</p>
          <p className="paragraph">something</p>
        </div>

        <div className="col-md-3">
          <h3 className="heading-tertiary">Something</h3>
          <p className="paragraph">something</p>
          <p className="paragraph">something</p>
          <p className="paragraph">something</p>
        </div>

        <div className="col-md-3">
          <h3 className="heading-tertiary">Something</h3>
          <p className="paragraph">something</p>
          <p className="paragraph">something</p>
          <p className="paragraph">something</p>
        </div>
      </div>
    </div>
  </section>
);

export default Footer;
