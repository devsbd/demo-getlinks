
export const handleErrorLogin = (response, router) => {
  if(response && response.status === 401){
    router.push('/login');
  }
}