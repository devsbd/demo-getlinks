import axios from "axios";
import constants from "src/utils/constants";

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_URL || "http://0.0.0.0:5000/api",
  headers: {
    "Content-Type": "application/json"
  }
});

instance.interceptors.request.use(config => {
  const Authorization = localStorage.getItem(constants.DEMO_TOKEN);
  if (Authorization) {
    config.headers["Authorization"] = Authorization;
  }
  return config;
});

export default instance;
