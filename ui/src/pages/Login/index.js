import React from "react";
import Form from "src/layouts/form";
import axios from "src/utils/axios";
import constants from "src/utils/constants";
import { Redirect } from "react-router-dom";

export default class LoginContainer extends React.Component {
  state = {
    messageError: "",
    isLoginSuccess: false,
  };
  submitting = false;
  handleLogin = ({ email, password }) => {
    if (this.submitting) return;
    axios
      .post("/user/login", {
        email,
        password
      })
      .then(this._handleResponse)
      .catch(this._handleError)
      .then(() => (this.submitting = false));
  };

  _handleResponse = async res => {
    if (res.status === 200) {
      await localStorage.setItem(constants.DEMO_TOKEN, res.data.token);
      await localStorage.setItem(constants.DEMO_USER, JSON.stringify(res.data));
      setTimeout(() => this.setState({ isLoginSuccess: true }), 500);
    }
  };

  _handleError = ({ response }) => {
    this.setState({
      messageError: response ? response.data.message : "Don't request to server"
    });
  };

  render() {
    const { messageError, isLoginSuccess } = this.state;
    if (isLoginSuccess) {
      return <Redirect to="/" />;
    }
    return (
      <div className="section-login-outer">
        <Form messageError={messageError} handleSubmit={this.handleLogin} />
      </div>
    );
  }
}
