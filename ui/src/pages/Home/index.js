import React from "react";

import Info from "src/layouts/info";
import Cards from "src/layouts/cards";
import axios from "src/utils/axios";
import { handleErrorLogin } from "src/utils/helper";

export default class Home extends React.Component {
  state = {
    data: []
  };

  getListPost = () => {
    axios.get("/post").then(res => {
      if (res.status === 200) {
        this.setState({ data: res.data });
      }
    })
    .catch( ({response}) => handleErrorLogin(response, this.props.history) );
  };

  componentDidMount() {
    this.getListPost();
  }

  render() {
    return (
      <React.Fragment>
        <Info />
        <Cards data={this.state.data} />
      </React.Fragment>
    );
  }
}
