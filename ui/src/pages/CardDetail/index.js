import React from "react";

import axios from "src/utils/axios";
import { handleErrorLogin } from "src/utils/helper";

export default class CardDetail extends React.Component {
  state = { data: null, isLoading: false };

  getInfoCard = () => {
    axios.get(`post/${this.props.match.params.id}`).then(res => {
      if (res.status === 200) {
        this.setState({
          data: res.data
        });
      }
    })
    .catch( ({response}) => handleErrorLogin(response, this.props.history) );
  };

  componentDidMount() {
    this.getInfoCard();
  }

  render() {
    const { data } = this.state;

    if (!data) return null;
    return (
      <div className="CardDetail u-margin-top-big  u-margin-bottom-big">
        <div className="container">
          <h2 className="heading-secondary">{data.title}</h2>
          <p dangerouslySetInnerHTML={{ __html: data.content }} />
        </div>
      </div>
    );
  }
}
