import React from "react";
import Home from "src/pages/Home";
import { mount } from "enzyme";
import axiosMock from "axios";
import { MemoryRouter } from "react-router";

beforeEach(() => {
  axiosMock.__mock.reset();
});

test("Get card details success", done => {
  const fakeData = {
    title: "test",
    content: "content",
    image: "test",
    _id: 1
  };
  const { get } = axiosMock.__mock.instance;
  get.mockImplementationOnce(() => {
    return Promise.resolve({
      status: 200,
      data: [fakeData]
    });
  });

  const Wrapper = mount(
    <MemoryRouter initialEntries={["/"]}>
      <Home />
    </MemoryRouter>
  );
  Wrapper.find("Home").update();

  setTimeout(() => {
    expect(get).toHaveBeenCalledTimes(1);
    expect(get).toHaveBeenLastCalledWith(`/post`);

    expect(Wrapper.find("Home").state().data.length).toEqual(1);
    Wrapper.unmount();
    return done();
  }, 100);
});
