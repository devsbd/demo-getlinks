import React from "react";
import LoginContainer from "src/pages/Login";
import { mount } from "enzyme";
import axiosMock from "axios";

beforeEach(() => {
  axiosMock.__mock.reset();
});

test("Login success", done => {
  const fakeData = {
    email: "email@gmail.com",
    password: "123"
  };

  const { post } = axiosMock.__mock.instance;
  post.mockImplementationOnce(() => {
    return Promise.resolve({
      data: {
        status: 200,
        data: fakeData
      }
    });
  });

  const Wrapper = mount(<LoginContainer />);

  Wrapper.find("input")
    .at(0)
    .simulate("change", { target: { value: fakeData.email } });
  Wrapper.find("input")
    .at(1)
    .simulate("change", { target: { value: fakeData.password } });

  Wrapper.find("button").simulate("click");

  setTimeout(() => {
    expect(post).toHaveBeenCalledTimes(1);
    expect(post).toHaveBeenLastCalledWith("/user/login", fakeData);
    Wrapper.unmount();
    return done();
  }, 100);
});

test("Show error when login fail", done => {
  const { post } = axiosMock.__mock.instance;
  post.mockImplementationOnce(() =>
    Promise.reject({
      response: {
        data: {
          message: "loi"
        }
      }
    })
  );
  const Wrapper = mount(<LoginContainer />);

  Wrapper.find("button").simulate("click");

  setTimeout(() => {
    Wrapper.find("form").update();
    expect(Wrapper.find(".form__alert").exists()).toBe(true);
    Wrapper.unmount();
    done();
  }, 100);
});
