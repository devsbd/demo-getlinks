import React from "react";
import Auth from "src/pages/Auth";
import { mount } from "enzyme";
import axiosMock from "axios";
import { MemoryRouter } from "react-router";
import constants from "src/utils/constants";
beforeEach(() => {
  axiosMock.__mock.reset();
});

test("Should render Main layouts when have token", done => {
  const fakeData = {
    title: "test",
    content: "content",
    image: "test",
    _id: 1
  };

  const { get } = axiosMock.__mock.instance;
  get.mockImplementationOnce(() => {
    return Promise.resolve({
      status: 200,
      data: [fakeData]
    });
  });

  window.localStorage.setItem(constants.DEMO_USER, "test");
  const Wrapper = mount(
    <MemoryRouter initialEntries={["/"]}>
      <Auth />
    </MemoryRouter>
  );

  setTimeout(() => {
    expect(get).toHaveBeenCalledTimes(1);
    expect(get).toHaveBeenLastCalledWith(`/post`);

    expect(Wrapper.find("Home").state().data.length).toEqual(1);
    Wrapper.unmount();
    window.localStorage.removeItem(constants.DEMO_USER);
    return done();
  }, 100);
});

test("Render Lgoin when user isn't login", done => {
  const Wrapper = mount(
    <MemoryRouter initialEntries={["/"]}>
      <Auth />
    </MemoryRouter>
  );

  setTimeout(() => {
    expect(Wrapper.find("LoginContainer").exists()).toBe(true);
    Wrapper.unmount();
    return done();
  }, 100);
});
