import React from "react";
import CardDetail from "src/pages/CardDetail";
import { mount } from "enzyme";
import axiosMock from "axios";

beforeEach(() => {
  axiosMock.__mock.reset();
});

test("Get card details success", done => {
  const fakeData = {
    title: "test",
    content: "content",
    image: "test",
    _id: 1
  };
  const { get } = axiosMock.__mock.instance;
  get.mockImplementationOnce(() => {
    return Promise.resolve({
      status: 200,
      data: [fakeData]
    });
  });

  const Wrapper = mount(
    <CardDetail match={{ params: { id: fakeData._id } }} />
  );
  Wrapper.update();

  setTimeout(() => {
    expect(get).toHaveBeenCalledTimes(1);
    expect(get).toHaveBeenLastCalledWith(`post/${fakeData._id}`);

    expect(Wrapper.state().data.length).toEqual(1);
    Wrapper.unmount();
    return done();
  }, 100);
});
