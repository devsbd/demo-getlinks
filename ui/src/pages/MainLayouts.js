import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import AppRoutes from "src/routes";
import Header from "src/layouts/header";
import Footer from "src/layouts/footer";
import "src/main.scss";

const MainLayouts = props => {
  const {
    isLogin,
    location: { pathname }
  } = props;
  if (!isLogin && pathname !== "/login") {
    return <Redirect exact to="/login" />;
  }

  return (
    <div>
      <Header />
      <Switch>
        {AppRoutes.map(route => (
          <Route {...props} {...route} exact={!!route.exact} />
        ))}
      </Switch>
      <Footer />
    </div>
  );
};

export default MainLayouts;
