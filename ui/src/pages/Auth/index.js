import React from "react";
import { Route, Switch } from "react-router-dom";
import constants from "src/utils/constants";

import LoginContainer from "../Login";
import MainLayouts from "../MainLayouts";

export default class Auth extends React.Component {
  state = {
    isLogin: false
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      isLogin: !!localStorage.getItem(constants.DEMO_USER)
    };
  }

  render() {
    const { isLogin } = this.state;

    return (
      <Switch>
        <Route exact path="/login" component={LoginContainer} />
        <MainLayouts {...this.props} isLogin={isLogin} />
      </Switch>
    );
  }
}
